package com.example.dagger2example.basics

object Constants {

    val MY_TAG = "Constants.MyTag"
    val DEBUG_ENDPOINT =
        "This is Example#1. BuildConfig.DEBUG=true, " + "This is the Example of Constructor Injection using dagger2"
    val PROD_ENDPOINT =
        "This is Example#1. BuildConfig.DEBUG=false," + "This is the Example of Constructor Injection using dagger2"
}
