package com.example.dagger2example.network

import javax.inject.Inject

class NetworkClient @Inject
constructor(private val mConnection: NetworkConnection) {

    fun fetchData(): String? {
        return mConnection.doReq()
    }
}