package com.example.dagger2example.network


import com.example.dagger2example.BuildConfig
import com.example.dagger2example.basics.Constants

import javax.inject.Inject

class NetworkConnection @Inject
constructor() {
    private var mEndpoint: String? = null

    init {

        if (BuildConfig.DEBUG) {
            this.mEndpoint = Constants.DEBUG_ENDPOINT
        } else {
            this.mEndpoint = Constants.PROD_ENDPOINT
        }

    }

    fun doReq(): String? {
        return this.mEndpoint
    }

}

