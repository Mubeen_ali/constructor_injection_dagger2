package com.example.dagger2example.di

import com.example.dagger2example.MainActivity
import com.example.dagger2example.di.DaggerMainViewModelInjector

import com.example.dagger2example.basics.MainViewModel
import dagger.Component


@Component
interface MainViewModelInjector {
    //provision method which is a getter method
    //return type is MainViewModel
    //abstract method which means body isn't provide here
    //these provision methods provide us dependencies in 2 ways
    //1) by getter methpd as shown below
    //2) they can inject some field
    //error is there which is due to the fact that dagger expects @inject to annonate
    //the constructtors that how to create mainViewModel
    //means we have to go to mainViewModel and perform constructor injection there
    //@component dy kr hmne dagger ko ye btaya h k kia krna hy or dagger ko ye pta
    // chal gya hy k ye ek top level factory hy or iss interface ko dagger compile
    // time p khud implement k create krygi or iss getMainViewModel ka jo code hoga
    // wo implememnt krke  khud generate krgi
    //means dagger ko humne ye btaya hy k kia krna hy kesy krna hy ye nh btaya
    //finally dagger DaggerMainViewModelInjector top level factory generate krygi
    //or prefix Dagger add hojaega is name k agy jb sary constructore @inject use
    //krlaingy properly
    //@component ko use krte hue hum ek top level abstract factory ya abstract component
    //define krte hain. sirf is interface ki base p dagger dependency graph creates krta or
    //or store krke rkhta hy
    abstract fun getMainViewModel(): MainViewModel


    //---------------------Field Injection---------------------------//

    //-----follow 3 steps for field injection,agr ese hi run kia jae tow null pointer
    //exceptions aaengi,q k field injection ese hi trigger nh hota isky liye separate
    // method bnana parta hy or manually trigger krna parta hy is field injection k process ko

    //    now follow 3 stepts. 1) MainViewModel can't be private,and use @Inject at top of it
    //    2) abstract  factory me jaa kr hme ek injector method create krsingy uski type
    //     void hogi or parameter me wo class pass hogi jiski field ko inject krwana hy like
    //     yaha main activity pass ki jaegi q k field MAinView model ko inject krwa rhy hain

//    fun InjectFields(mainActivity: MainActivity)

}
