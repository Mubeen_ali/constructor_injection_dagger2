package com.example.dagger2example


import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

import com.example.dagger2example.basics.MainViewModel
import com.example.dagger2example.network.NetworkClient
import com.example.dagger2example.network.NetworkConnection

import androidx.appcompat.app.AppCompatActivity

import javax.inject.Inject

import com.example.dagger2example.basics.Constants.MY_TAG
import com.example.dagger2example.di.DaggerMainViewModelInjector

class MainActivity : AppCompatActivity() {

    //    Const Injection
     var mMainViewModel: MainViewModel? = null

    //-----------------//

    //Field Injwction
//    @Inject
//     var mMainViewModel: MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.textView) as TextView
        val button = findViewById<Button>(R.id.connect) as Button
        //------------------------------Cont Injection-----------------------------------------//
        mMainViewModel=DaggerMainViewModelInjector.create().getMainViewModel()
        //--------------//
        //----------------Field Injection-----------------//
//        DaggerMainViewModelInjector.create().InjectFields(this)
        //now after creating this injector method InjectFields() mMainViewModel
        //isn't having null pointer exceptions

        //        NetworkConnection connection = new NetworkConnection();
        //        NetworkClient client = new NetworkClient(connection);
        //
        //        mMainViewModel = new MainViewModel(client);

        button.setOnClickListener({ view ->
            Log.d(MY_TAG, "onCreate: Data fetched")
            textView.setText(mMainViewModel!!.fetchData())
        })

    }
}
