package com.example.dagger2example.basics


import com.example.dagger2example.network.NetworkClient

import javax.inject.Inject

class MainViewModel
//this annotation comes from javax package
//bcz it;s a java specification request which is known as jsr330
//java specification request 330 means under jsr330 these sort of annotation
// are standarize means different di-frameworks use it all means if u want to change
// your di-frame work during our project work so u can
@Inject
constructor(private val mClient: NetworkClient) {

    fun fetchData(): String? {
        return mClient.fetchData()
    }
}
